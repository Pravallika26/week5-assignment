 create database normalForms;
 use normalforms;
  create table Project_Details(projectCode varchar(20) primary key, projectName varchar(20),projectManager varchar(20),projectBudget int(70));
  Insert into Project_details values('PC010','Reservation System','Mr. Ajay',120500),('PC011','HR System','Mrs. Charu',500500),('PC012','Attenndance System','Mr. Rajesh',710700);
  select * from Project_details;
  
   Create table Employee(ProjectCode varchar(20),EmployeeNo varchar(20) Primary key,EmployeeName varchar(20),DepartmentNo varchar(10),DepartmentName varchar(20),HourlyRate double(4,2));
   Insert into Employee values('PC010','S100','Mohan','D03','Database',21.00),('PC010','S101','Vipul','D02','Testing',16.50),('PC010','S102','Riyaz','D01','IT',22.00),('PC011','S103','Pavan','D03','Database',18.00),('PC011','S104','Jitendra','D02','Testing',17.00),('PC011','S315','Pooja','D01','IT',23.50),('PC012','S137','Rahul','D03','Database',21.50),('PC012','S218','Avneesh','D02','Testing',15.50),('PC012','S109','Vikas','D01','IT',20.50);
   select * from Employee;
   
   create table Employee_details(EmployeeNo varchar(20) Primary key,EmployeeName varchar(20),DepartmentNo varchar(10));
   INSERT INTO Employee_details values('S100','Mohan','D03'),('S101','Vipul','D02'),('S102','Riyaz','D01'),('S103','Pavan','D03'),('S104','Jitendra','D02'),('S315','Pooja','D01'),('S137','Rahul','D03'),('S218','Avneesh','D02'),('S109','Vikas','D01');
   select * from Employee_details;
   
   create table hours(EmployeeNo varchar(20), HourlyRate double(4,2));
    Insert into Hours values('S100',21.00),('S101',16.50),('S102',22.00),('S103',18.00),('S104',17.00),('S315',23.50),('S137',21.50),('S218',15.50),('S109',20.50);
    select * from Hours;
    
     create table Department(DepartmentNo varchar(20),DepartmentName varchar(20));
     Insert into department values('D01','IT'),('D02','Testing'),('D03','DataBase');
     select * from department;