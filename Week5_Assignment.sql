create database travelonmind;
use travelonmind;

create table Passenger
 (Passenger_name varchar(20), 
  Category           varchar(20),
  Gender             varchar(20),
  Boarding_City      varchar(20),
  Destination_City   varchar(20),
  Distance           int,
  Bus_Type           varchar(20)
);
describe passenger;

create table Price
(
	Bus_Type   varchar(20),
	Distance   int,
	Price      int
);
describe price;

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

-- females and males travelled for a min distance of 600 kms
select 'F' as Gender , count(Gender) from passenger where Gender='F' and Distance >= 600 union all select 'M' as Gender , count(Gender) from passenger where Gender='M' and Distance >= 600;

-- min ticket price for sleeper bus
Select min(price) as Least_Price from price where bus_type = "Sleeper";

-- passenger names starting with 's'
select passenger_name from passenger where passenger_name like 's%';

-- price charged for each passenger displaying required columns
 select p.Passenger_name,p.Boarding_city,p.Destination_city,p.Bus_Type,pr.Price from Passenger p, Price pr where p.Bus_Type=pr.Bus_Type and p.Distance=pr.Distance;
 
-- passenger name and ticket price travelled in sitting bus for a distance of 1000kms
select p.Passenger_name,pr.Price from passenger p inner join price pr on p.Bus_Type=pr.Bus_Type and p.Distance=pr.Distance where p.Bus_Type="Sitting" and p.Distance=1000;  

-- sitting and sleeper bus charge for pallavi from Bangalore to Panaji
select 'Bangalore' as Boarding_City, 'Panaji' as Destination_City,pr.Bus_Type,pr.Price from Price pr inner join Passenger p on pr.Distance = p.Distance where Passenger_name = "Pallavi";

-- distances from passenger table which are unique in descending order
select Distance from passenger group by Distance  having count(Distance)=1 order by Distance desc;
/* we can also do using distinct keyword
select distinct Distance from passenger order by Distance desc;    */

-- passenger name and percent of distance without using user variables
select Passenger_name,Distance*100/(select sum(Distance) from passenger) as Percentage_of_Distance from passenger;

-- view to see all passengers travelled in AC bus
create view VW_AC_Passengers as select Passenger_name,Category from passenger where Category="AC";
select * from VW_AC_Passengers;

-- stored procedure to find total passengers using sleeper buses
delimiter []
create procedure totalCount()
begin
select count(*) as total_count from passenger where Bus_Type = 'sleeper';
end []
call totalCount;

/* for displaying total passenger names
delimiter []
create procedure totalPassengers()
begin
select Passenger_Name from passenger where Bus_type = 'sleeper';
end []
call totalPassengers;      */

-- display 5 records at one time
select * from passenger order by Passenger_name limit 5;